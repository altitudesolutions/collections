import { Queue } from "../src/queue";

/* tslint:disable:no-magic-numbers*/

describe("collections.Queue", () => {
    it("Should maintain internal array in ascending order", () => {
        const c = new Queue<number>();
        for (let i = 0; i < 5; i += 1) {
            c.enqueue(i);
        }
        expect(c.toArray()).toEqual([0, 1, 2, 3, 4]);
    });

    it("for-of should iterate collection in FIFO sequence", () => {
        const c = new Queue<number>();
        for (let i = 0; i < 5; i += 1) {
            c.enqueue(i);
        }
        let itemIdx = 0;
        for (const item of c) {
            expect(item).toEqual(itemIdx);
            itemIdx += 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(5);
    });

    it("Should dequeue in FIFO sequence", () => {
        const c = new Queue<number>();
        for (let i = 0; i < 5; i += 1) {
            c.enqueue(i);
        }

        let itemIdx = 0;
        while (c.count > 0) {
            expect(c.dequeue()).toEqual(itemIdx);
            itemIdx += 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(5);
    });

    it("Should dequeue all in FIFO sequence", () => {
        const c = new Queue<number>();
        for (let i = 0; i < 5; i += 1) {
            c.enqueue(i);
        }

        const c1 = c.dequeueAll();

        let itemIdx = 0;
        while (c1.length > 0) {
            expect(c1.shift()).toEqual(itemIdx);
            itemIdx += 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(5);
    });
});
