import { Stack } from "../src/stack";

/* tslint:disable:no-magic-numbers*/

describe("collections.Stack", () => {
    it("Should maintain internal array in ascending order", () => {
        const c = new Stack<number>();
        for (let i = 0; i < 5; i += 1) {
            c.push(i);
        }
        expect(c.toArray()).toEqual([0, 1, 2, 3, 4]);
    });

    it("for-of should iterate collection in LIFO sequence", () => {
        const c = new Stack<number>();
        for (let i = 0; i < 5; i += 1) {
            c.push(i);
        }
        let itemIdx = 4;
        for (const item of c) {
            expect(item).toEqual(itemIdx);
            itemIdx -= 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(-1);
    });

    it("Should pop in LIFO sequence", () => {
        const c = new Stack<number>();
        for (let i = 0; i < 5; i += 1) {
            c.push(i);
        }

        let itemIdx = 4;
        while (c.count > 0) {
            expect(c.pop()).toEqual(itemIdx);
            itemIdx -= 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(-1);
    });

    it("Should pop all in LIFO sequence", () => {
        const c = new Stack<number>();
        for (let i = 0; i < 5; i += 1) {
            c.push(i);
        }

        const c1 = c.popAll();

        let itemIdx = 4;
        while (c1.length > 0) {
            expect(c1.pop()).toEqual(itemIdx);
            itemIdx -= 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(-1);
    });
});
