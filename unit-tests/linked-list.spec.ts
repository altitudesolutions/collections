import { IListItem, LinkedList } from "../src/linked-list";

/* tslint:disable:no-magic-numbers*/

describe("collections.LinkedList", () => {
    it("for-of should iterate collection in proper sequence", () => {
        const c = new LinkedList<number>();
        for (let i = 0; i < 5; i += 1) {
            c.append(i);
        }
        let itemIdx = 0;
        for (const item of c) {
            expect(item).toEqual(itemIdx);
            itemIdx += 1;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(5);
    });

    it("Should iterate from first to last in proper sequence", () => {
        const c = new LinkedList<number>();
        for (let i = 0; i < 5; i += 1) {
            c.append(i);
        }

        let itemIdx = 0;
        let item = c.first;
        while (item != undefined) {
            expect(item.value).toEqual(itemIdx);
            itemIdx += 1;
            item = item.next;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(5);
    });

    it("Should iterate from last to first in proper sequence", () => {
        const c = new LinkedList<number>();
        for (let i = 0; i < 5; i += 1) {
            c.append(i);
        }

        let itemIdx = 4;
        let item = c.last;
        while (item != undefined) {
            expect(item.value).toEqual(itemIdx);
            itemIdx -= 1;
            item = item.prev;
        }

        expect(itemIdx)
            .withContext("Did not iterate all items in collection")
            .toEqual(-1);
    });

    it("Should insert before last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.insertBefore(3, i2);

        expectArray(c, [1, 3, 2]);
        expectFirst(c, i1);
        expectLast(c, i2);
    });

    it("Should insert after first", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.insertAfter(3, i1);

        expectArray(c, [1, 3, 2]);
        expectFirst(c, i1);
        expectLast(c, i2);
    });

    it("Should insert before first", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.insertBefore(2, i1);

        expect(i1.prev).toBe(i2);
        expect(i1.next).toBeUndefined();
        expect(i2.prev).toBeUndefined();
        expect(i2.next).toBe(i1);
        expectFirst(c, i2);
        expectLast(c, i1);
        expectArray(c, [2, 1]);
    });

    it("Should insert after last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.insertAfter(2, i1);

        expect(i1.next).toBe(i2);
        expect(i1.prev).toBeUndefined();
        expect(i2.next).toBeUndefined();
        expect(i2.prev).toBe(i1);
        expectFirst(c, i1);
        expectLast(c, i2);
        expectArray(c, [1, 2]);
    });

    //
    it("Should move before last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.append(3);
        const i4 = c.append(4);
        c.moveBefore(i2, i4);

        expectArray(c, [1, 3, 2, 4]);
        expectFirst(c, i1);
        expectLast(c, i4);
    });

    it("Should move before first", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        c.append(2);
        const i3 = c.append(3);
        const i4 = c.append(4);
        c.moveBefore(i3, i1);

        expectArray(c, [3, 1, 2, 4]);
        expectFirst(c, i3);
        expectLast(c, i4);
    });

    it("Should move after first", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        c.append(2);
        const i3 = c.append(3);
        const i4 = c.append(4);
        c.moveAfter(i3, i1);

        expectArray(c, [1, 3, 2, 4]);
        expectFirst(c, i1);
        expectLast(c, i4);
    });

    it("Should move after last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.append(3);
        const i4 = c.append(4);
        c.moveAfter(i2, i4);

        expectArray(c, [1, 3, 4, 2]);
        expectFirst(c, i1);
        expectLast(c, i2);
    });

    it("Should move first before last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        const i3 = c.append(3);
        c.moveBefore(i1, i3);

        expectArray(c, [2, 1, 3]);
        expectFirst(c, i2);
        expectLast(c, i3);
    });

    it("Should move last before first", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        const i3 = c.append(3);
        c.moveBefore(i3, i1);

        expectArray(c, [3, 1, 2]);
        expectFirst(c, i3);
        expectLast(c, i2);
    });

    it("moveBefore should swap last and first", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.moveBefore(i2, i1);

        expectArray(c, [2, 1]);
        expectFirst(c, i2);
        expectLast(c, i1);
    });

    it("moveAfter should swap first and last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.moveAfter(i1, i2);

        expectArray(c, [2, 1]);
        expectFirst(c, i2);
        expectLast(c, i1);
    });

    it("Prepended item should always be first", () => {
        const count = 5;
        const c = new LinkedList<number>();
        for (let idx = 1; idx <= count; idx += 1) {
            const item = c.prepend(idx);
            expectFirst(c, item);
        }
        expectArray(c, [5, 4, 3, 2, 1]);
    });

    it("Appended item should always be last", () => {
        const count = 5;
        const c = new LinkedList<number>();
        for (let idx = 1; idx <= count; idx += 1) {
            const item = c.append(idx);
            expectLast(c, item);
        }
        expectArray(c, [1, 2, 3, 4, 5]);
    });

    it("Item prepended to empty list should be both first and last", () => {
        const c = new LinkedList<number>();
        const item = c.prepend(1);

        expectArray(c, [1]);
        expectFirst(c, item);
        expectLast(c, item);
    });

    it("Item appended to empty list should be both first and last", () => {
        const c = new LinkedList<number>();
        const item = c.append(1);

        expectArray(c, [1]);
        expectFirst(c, item);
        expectLast(c, item);
    });

    it("Prepending to a single item list should make first last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.prepend(2);

        expectArray(c, [2, 1]);
        expectFirst(c, i2);
        expectLast(c, i1);
    });

    it("moveFirst should swap first and last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.moveFirst(i2);

        expectArray(c, [2, 1]);
        expectFirst(c, i2);
        expectLast(c, i1);
    });

    it("moveLast should swap first and last", () => {
        const c = new LinkedList<number>();
        const i1 = c.append(1);
        const i2 = c.append(2);
        c.moveLast(i1);

        expectArray(c, [2, 1]);
        expectFirst(c, i2);
        expectLast(c, i1);
    });
});

function expectArray(c: LinkedList<number>, array: number[]): void {
    expect(c.count)
        .withContext("Unexpected count")
        .toEqual(array.length);
    expect(c.toArray())
        .withContext("ListItem.toArray did not match expected array")
        .toEqual(array);
}

function expectFirst(c: LinkedList<number>, item: IListItem<number>): void {
    expect(c.first)
        .withContext("Unexpected first item")
        .toBe(item);
}

function expectLast(c: LinkedList<number>, item: IListItem<number>): void {
    expect(c.last)
        .withContext("Unexpected last item")
        .toBe(item);
}
