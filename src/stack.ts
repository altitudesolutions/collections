import { invalidOperation } from "@altitude/error-message";

import { reverseArrayIterator } from "./array-iterator";
import { INativeIterator } from "./native-iterator";
/**
 * Implements a simple Stack by encapsulating an Array.
 *
 * The internal array is maintained such that the item at the top of the Stack
 * may be removed from the array using its native `pop` method.
 */
export class Stack<T> {
    /**
     * The number of items in the stack.
     */
    get count(): number {
        return this._array.length;
    }
    private _array: T[] = [];

    constructor(array?: T[]) {
        this._array = array != undefined ? array : [];
    }

    /**
     * Returns a `NativeIterator` for the stack.
     * This property enables seamless use of Typescript/Javascript for/of loop to the items in the stack.
     */
    [Symbol.iterator](): INativeIterator<T> {
        return reverseArrayIterator(this._array);
    }

    /**
     * Removes all items from the stack.
     */
    clear() {
        this._array = [];
    }

    /**
     * Returns the object at the top of the stack without removing it.
     */
    peek(): T {
        if (this.count === 0) {
            throw new Error(
                invalidOperation("Attempt to peek an empty Stack.")
            );
        }

        return this._array[this._array.length - 1];
    }

    /**
     * Pops an item from the stack.
     */
    pop(): T {
        if (this.count === 0) {
            throw new Error(
                invalidOperation("Attempt to pop from an empty Stack.")
            );
        }

        return this._array.pop();
    }

    /**
     * Pops all the items in the stack to an array, such that items can be popped from
     * the result array using its native `pop` method.
     */
    popAll(): T[] {
        const r = this._array;
        this._array = [];

        return r;
    }

    /**
     * Pushes an item onto the stack.
     * @param item The item to push.
     */
    push(item: T) {
        this._array.push(item);
    }

    /**
     * Pushes the items in an array. The item with the lowest array index is pushed first.
     *
     * @param items The items to enqueue.
     */
    pushAll(items: T[]): void {
        if (items == undefined || items.length === 0) return;

        items.forEach(item => {
            this._array.push(item);
        });
    }

    /**
     * Returns the contents of the stack as an array.
     */
    toArray(): T[] {
        return this._array.slice(0);
    }
}
