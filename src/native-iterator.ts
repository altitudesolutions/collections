export interface INativeIterator<T> {
    next(): { done: boolean; value: T };
}
