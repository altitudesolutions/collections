import { invalidOperation } from "@altitude/error-message";

import { arrayIterator } from "./array-iterator";
import { INativeIterator } from "./native-iterator";
/**
 * Implements a simple Queue by encapsulating an Array.
 *
 * The internal array is maintained such that the item at the front of the Queue
 * may be removed from the array using its native `shift` method.
 */
export class Queue<T> {
    /**
     * The number of items in the queue.
     */
    get count(): number {
        return this._array.length;
    }
    private _array: T[];

    constructor(array?: T[]) {
        this._array = array != undefined ? array : [];
    }

    /**
     * Returns a `NativeIterator` for the queue.
     * This property enables seamless use of Typescript/Javascript for/of loop to the items in the queue.
     */
    [Symbol.iterator](): INativeIterator<T> {
        return arrayIterator(this._array);
    }

    /**
     * Removes all items from the queue.
     */
    clear(): void {
        this._array = [];
    }

    /**
     * Removes the item at the front of the queue.
     */
    dequeue(): T {
        if (this.count === 0) {
            throw new Error(
                invalidOperation("Attempt to dequeue from an empty Queue.")
            );
        }

        return this._array.shift();
    }

    /**
     * Dequeues all the items in the queue to an array, such that items can be dequeued from
     * the result array using its native `shift` method.
     */
    dequeueAll(): T[] {
        const r = this._array;
        this._array = [];

        return r;
    }

    /**
     * Adds an item to the queue.
     * @param item The item to add
     */
    enqueue(item: T): void {
        this._array.push(item);
    }

    /**
     * Enqueues the items in an array.
     *
     * @param items The items to enqueue.
     */
    enqueueAll(items: T[]): void {
        if (items == undefined || items.length === 0) return;

        items.forEach(item => {
            this._array.push(item);
        });
    }

    /**
     * Returns the object at the front of the queue without removing it.
     */
    peek(): T {
        if (this.count === 0) {
            throw new Error(
                invalidOperation("Attempt to peek an empty Queue.")
            );
        }

        return this._array[this.count - 1];
    }

    /**
     * Returns the contents of the queue as an array.
     */
    toArray(): T[] {
        return this._array.slice(0);
    }
}
