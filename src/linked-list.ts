import { argumentNull } from "@altitude/error-message";

import { INativeIterator } from "./native-iterator";
/**
 * A member of a `LinkedList`.
 */
export interface IListItem<T> {
    /**
     * A reference to the next `ListItem`, or `undefined` if this item is the last in the list.
     */
    readonly next: IListItem<T>;
    /**
     * A reference to the previous `ListItem`, or `undefined` if this item is the first in the list.
     */
    readonly prev: IListItem<T>;
    /**
     * The value of the item.
     */
    readonly value: T;
}
/**
 * A doubly-linked list.
 */
export class LinkedList<T> {
    /**
     * Returns the number of items in the list.
     */
    get count(): number {
        return this._count;
    }
    /**
     * Returns this first item in the list, or `undefined` if the list is empty.
     */
    get first(): IListItem<T> {
        return this._first;
    }
    /**
     * Returns this last item in the list, or `undefined` if the list is empty.
     */
    get last(): IListItem<T> {
        return this._last;
    }
    private _count = 0;
    private _first: IListItemInt<T>;
    private _last: IListItemInt<T>;
    /**
     * Returns a `NativeIterator` for the list.
     * This property enables seamless use of Typescript/Javascript for/of loop to iterate the list items.
     */
    [Symbol.iterator](): INativeIterator<T> {
        let current = this.first;

        return {
            next() {
                const done = current == undefined;
                const r = { done, value: undefined };
                if (!done) {
                    r.value = current.value;
                    current = current.next;
                }

                return r;
            }
        };
    }
    /**
     * Adds a value to the end of list.
     * @param value The item to add.
     */
    append(value: T): IListItem<T> {
        const newItem = listItemInt(this, value);
        newItem.prev = this._last;
        if (this._count > 0) {
            this._last.next = newItem;
        } else {
            this._first = newItem;
        }

        this._last = newItem;
        this._count += 1;

        return newItem;
    }
    /**
     * Removes all items from the list.
     */
    clear() {
        this._first = undefined;
        this._last = undefined;
        this._count = 0;
    }
    /**
     * Looks up a value in the list and if it exists returns its associated `ListItem`, otherwise returnd `undefined`.
     * @param value the value to lookup.
     */
    find(compareFn: (value: T) => boolean): IListItem<T> {
        let item = this.first;
        while (item != undefined) {
            if (compareFn(item.value)) return item;
            item = item.next;
        }

        return undefined;
    }
    /**
     * Inserts a value immediately after a reference `ListItem`.
     * The reference item must already belong to the list.
     *
     * @param value The value to insert.
     * @param other The reference item.
     */
    insertAfter(value: T, other: IListItem<T>): IListItem<T> {
        if (other == undefined) throw new Error(argumentNull("other"));
        if (value == undefined) throw new Error(argumentNull("value"));
        validate(this, <any>other, "other");
        const newItem = listItemInt(this, value);
        after(newItem, <any>other);
        this._count += 1;

        return newItem;
    }
    /**
     * Inserts a value immediately before a reference `ListItem`.
     * The reference item must already belong to the list.
     *
     * @param value The value to insert.
     * @param other The reference item.
     */
    insertBefore(value: T, other: IListItem<T>): IListItem<T> {
        if (other == undefined) throw new Error(argumentNull("other"));
        if (value == undefined) throw new Error(argumentNull("value"));
        validate(this, <any>other, "other");
        const newItem = listItemInt(this, value);
        before(newItem, <any>other);
        this._count += 1;

        return newItem;
    }
    /**
     * Moves a `ListItem` to the postition immediately after a reference `ListItem`.
     * Both items must already belong to the list.
     *
     * @param item The item to move.
     * @param other The reference item.
     */
    moveAfter(item: IListItem<T>, other: IListItem<T>) {
        if (other == undefined) throw new Error(argumentNull("other"));
        if (item == undefined) throw new Error(argumentNull("item"));
        validate(this, <any>item, "item");
        validate(this, <any>other, "other");
        after(<any>item, <any>other);
    }
    /**
     * Moves a `ListItem` to the postition immediately before a reference `ListItem`.
     * Both items must already belong to the list.
     *
     * @param item The item to move.
     * @param other The reference item.
     */
    moveBefore(item: IListItem<T>, other: IListItem<T>) {
        if (other == undefined) throw new Error(argumentNull("other"));
        if (item == undefined) throw new Error(argumentNull("item"));
        validate(this, <any>item, "item");
        validate(this, <any>other, "other");
        before(<any>item, <any>other);
    }
    /**
     * Moves a `ListItem` to the start of the list.
     * The item must already belong to the list.
     *
     * @param item The item to move.
     */
    moveFirst(item: IListItem<T>) {
        if (item == undefined) throw new Error(argumentNull("item"));
        validate(this, <any>item, "item");
        if (item === this.first) return;
        const itemInt: IListItemInt<T> = <any>item;
        detach(itemInt);
        // The list has at least one other member
        this._first.prev = itemInt;
        itemInt.next = this._first;
        this._first = itemInt;
    }
    /**
     * Moves a `ListItem` to the end of the list.
     * The item must already belong to the list.
     *
     * @param item The item to move.
     */
    moveLast(item: IListItem<T>) {
        if (item == undefined) throw new Error(argumentNull("item"));
        validate(this, <any>item, "item");
        if (item === this.last) return;
        const itemInt: IListItemInt<T> = <any>item;
        detach(itemInt);
        // The list has at least one other member
        this._last.next = itemInt;
        itemInt.prev = this._last;
        this._last = itemInt;
    }
    /**
     * Adds a value to the start of list.
     * @param value The item to add.
     */
    prepend(value: T): IListItem<T> {
        const newItem = listItemInt(this, value);
        newItem.next = this._first;
        if (this._count > 0) {
            this._first.prev = newItem;
        } else {
            this._last = newItem;
        }

        this._first = newItem;
        this._count += 1;

        return newItem;
    }
    /**
     * Removes an item from the list.
     * The item must already belong to the list.
     * @param item the item to remove.
     */
    remove(item: IListItem<T>) {
        if (item == undefined) throw new Error(argumentNull("item"));
        validate(this, <any>item, "item");
        detach(<any>item);
        // Dissociate the item from this instance.
        (<any>item)._owner = undefined;
        this._count -= 1;
    }
    /**
     * Returns the contents of the list as an array.
     */
    toArray(): T[] {
        const r: T[] = [];
        let item = this.first;
        while (item != undefined) {
            r.push(item.value);
            item = item.next;
        }

        return r;
    }
}

interface ILinkedListInt<T> {
    _first: IListItemInt<T>;
    _last: IListItemInt<T>;
}

interface IListItemInt<T> {
    _owner: ILinkedListInt<T>;
    next: IListItemInt<T>;
    prev: IListItemInt<T>;
    value: T;
}

function listItemInt<T>(owner: LinkedList<T>, item: T): IListItemInt<T> {
    return {
        _owner: <any>owner,
        next: undefined,
        prev: undefined,
        value: item
    };
}

function validate(
    owner: LinkedList<any>,
    item: IListItemInt<any>,
    paramName: string
) {
    if (item._owner !== <any>owner) {
        throw new Error(
            `The ListItem "${paramName}" is not associated with the LinkedList instance.`
        );
    }
}
/**
 * Detaches a `ListItemInt` from the linked list.
 */
function detach<T>(target: IListItemInt<T>) {
    const owner = target._owner;
    if (target.next != undefined) {
        target.next.prev = target.prev;
    }

    if (target.prev != undefined) {
        target.prev.next = target.next;
    }

    if (target === owner._first) {
        owner._first = target.next;
    }

    if (target === owner._last) {
        owner._last = target.prev;
    }
    target.prev = undefined;
    target.next = undefined;
}
/**
 * Moves a specfified target item to a position before a specified reference item.
 */
function before<T>(target: IListItemInt<T>, ref: IListItemInt<T>) {
    detach(target);
    target.prev = ref.prev;
    target.next = ref;
    ref.prev = target;
    if (target.prev != undefined) {
        target.prev.next = target;
    } else {
        target._owner._first = target;
    }
}
/**
 * Moves a specfified target item to a position after a specified reference item.
 */
function after<T>(target: IListItemInt<T>, ref: IListItemInt<T>) {
    detach(target);
    target.prev = ref;
    target.next = ref.next;
    ref.next = target;
    if (target.next != undefined) {
        target.next.prev = target;
    } else {
        target._owner._last = target;
    }
}
