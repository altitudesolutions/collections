import { argumentNull } from "@altitude/error-message";

import { INativeIterator } from "./native-iterator";

export function arrayIterator<T>(array: T[]): INativeIterator<T> {
    if (array == undefined) {
        throw new Error(argumentNull("array"));
    }

    return <any>array[Symbol.iterator]();
}

export function reverseArrayIterator<T>(array: T[]): INativeIterator<T> {
    if (array == undefined) {
        throw new Error(argumentNull("array"));
    }
    let idx = array.length - 1;

    return {
        next() {
            const done = idx < 0;
            const r = { done, value: undefined };
            if (!done) {
                r.value = array[idx];
                idx -= 1;
            }

            return r;
        }
    };
}
