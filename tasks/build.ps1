Write-Host "Executing Typescript build task"
$ProjectPath = $args[0]
if ([string]::IsNullOrEmpty($ProjectPath) -or !(Test-Path $ProjectPath -PathType Container)) {
    Write-Host "Missing or invalid project path "$ProjectPath
    exit;
}

$DistPath = "$ProjectPath\dist"
if (Test-Path $DistPath -PathType Container) {
    Write-Host "Cleaning the build directory $DistPath"
    Remove-Item $DistPath\*.* -recurse
}

Write-Host "Typescript version:" 
invoke-expression -Command "$ProjectPath\node_modules\.bin\tsc.cmd --version"
$Cmd = "$ProjectPath\node_modules\.bin\tsc.cmd -p $ProjectPath\tsconfig.json"
Write-Host "Performing Typescript compilaton:"
Write-Host $Cmd
invoke-expression -Command $Cmd
